'''
Created on 23 nov. 2020

@author: Cesar
'''
class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

class Burbuja:
    def ordenacionBurbuja1(self, numeros):
        for i in range(1,len(numeros)):
            for j in range(0,len(numeros)-i-1):
                
                if(numeros[j+1]<numeros[j]):
                    aux = numeros[j]
                    numeros[j] = numeros[j+1]
                    numeros[j+1]=aux
    

    def ordenamientoBurbuja2(self, numeros):
        i = 1
        o = False
        while(i<len(numeros)):
            i = i +1
            o = True
            for j in range(0,len(numeros)-i-1):
                o = False
                aux = numeros[j]
                numeros[j]=numeros[j+1]
                numeros[j+1]=aux
    
    def ordenamientoBurbuja3(self, numeros):
        i = 1
        while(i<len(numeros)):
            i = i+1
            o = True
            for j in range(0,len(numeros)-i):
                if(numeros[j]>numeros[j+1]):
                    o = False
                    aux = numeros[j]
                    numeros[j]=numeros[j+1]
                    numeros[j+1]=aux
                    

class Seleccion:
    def ordenamienotSeleccion(self,numeros):
        for i in range(len(numeros)):
            for j in range(i,len(numeros)):
                if(numeros[i]>numeros[j]):
                    minimo = numeros[i]
                    numeros[i] = numeros[j]
                    numeros[j]=minimo
class Insercion:
    def ordenarInsercion(self,numeros):
        aux = 0  
        for i in range(1,len(numeros)):
            aux = numeros[i]
            j = (i-1)
            while(j>=0 and numeros[j]>aux):
                numeros[j+1]=numeros[j]
                numeros[j]=aux
                j-=1

class QuickSort:
    def quicksort(self,numeros,izq,der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0
        while(i<j):
            while(numeros[i]<=pivote and i <j):
                i = i +1
            while(pivote < numeros[j]):
                j = j-1
            if(i<j):
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux
        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if(izq < j-1):
            self.quicksort(numeros, izq, j-1)
        if(j+1<der):
            self.quicksort(numeros, j+1, der)
            
class Shellsort:
    def shellsort(self,numeros):
        intervalo = len(numeros)/2
        intervalo = int(intervalo)
        while(intervalo>0):
            for i in range(int(intervalo),len(numeros)):
                j = i-int(intervalo)
                while(j>=0):
                    k = j + int(intervalo)
                    if(numeros[j]<=numeros[k]):
                        j -=1
                    else:
                        aux = numeros[j]
                        numeros[j] = numeros[k]
                        numeros[k] = aux
                        j-=int(intervalo)

            intervalo = int(intervalo)/2
class RadixSort:
    def radixOrder(self, numeros,exp1):
        n = len(numeros)
        cantidadDatos = [0] * (n)
        contar = [0]*(10)
        
        for i in range(0,n):
            indice = (numeros[i]/exp1)
            contar[int((indice)%10)]+=1
        for i in range(1,10):
            contar[i]+=contar[i-1]
        i = n-1
        while(i>=0):
            indice = (numeros[i]/exp1)
            cantidadDatos[contar[int((indice)%10)]-1]=numeros[i]
            contar[int((indice)%10)]-=1
            i -=1
        i = 0
        for i in range(0,len(numeros)):
            numeros[i]=cantidadDatos[i]
    
    def radix(self,numeros):
        max1 = max(numeros)
        exp = 1
        while(max1/exp)>0:
            RadixSort.radixOrder(self, numeros, exp)
            exp *= 10

class Intercalacion:
    @staticmethod
    def interc(a1:list, a2:list) -> list:
        
        a3 : list = []
        cont : int = 0
        cont2 : int = 0

        while len(a1)!=cont and len(a2)!=cont2:

            if a1[cont]<=a2[cont2]:
                a3.append(a1[cont])
                cont+=1
            else:
                a3.append(a2[cont2])
                cont2+=1

        while len(a1)!=cont:
            a3.append(a1[cont])
            cont+=1

        while len(a2)!=cont2:
            a3.append(a2[cont2])
            cont2+=1

        return a3



class MezclaDirecta:
    def mezclaDirecta(self,numeros):
        mitad=len(numeros)//2
        
        if len(numeros)>=2:
            arregloIz=numeros[mitad:]
            arregloDer=numeros[:mitad]

            numeros.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
            
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    numeros.append(arregloIz.pop(0))
                else:
                    numeros.append(arregloDer.pop(0))
          
            while len(arregloIz)>0:
                numeros.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                numeros.append(arregloDer.pop(0))
        
        return numeros
    
    def mezclaDirecta2(self, array):
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
               
class MezclaNatural:
    def mezclaNatural(self,numeros):
        m = MezclaDirecta()
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                if(der<=derecho):
                    m.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
    


iii = Intercalacion()
md = MezclaDirecta()
mn = MezclaNatural()
c = Corrector()
s = Seleccion()
ins = Insercion()
q = QuickSort()
b = Burbuja()
ss = Shellsort()
r = RadixSort()
numeros = [3,5,10,1,12,15,16,56,98,23,109,53,202]
aux = [58,77,85,94,102,134,154,167,207,320]
numeros3 = [5,10,15,20,24,30,32,43]
opcion = 0

while(opcion != 10):
    numeros2 = numeros.copy()
    aux2 = aux.copy()
    print("======================== MENU =========================")
    print("Digite 1 pasa usar el metodo de ordenamiento BURBUJA")
    print("Digite 2 para usar el metodo de ordenamiento INSERCION")
    print("Digite 3 para usar el metodo de ordenamiento SELECCION")
    print("Digite 4 para usar el metodo de ordenamiento QUICKSORT")
    print("Digite 5 para usar el metodo de ordenamiento SHELLSORT")
    print("Digite 6 para usar le metodo de ordenamiento RADIXSORT")
    print("Digite 7 para usar el metodo de ordenamiento INTERCALACIÓN")
    print("Digite 8 para usar el metodo de ordenamiento MEZCLA DIRECTA")
    print("Digite 9 para usar el metodo de ordenamiento MEZCLA NATURAL")
    print("Digite 10 para ***SALIR***")
    opcion = c.correcion()
    
    if(opcion == 1):
        print("Digite 1 para usar el metodo de Burbuja 1")
        print("Digite 2 para usar el metodo de Burbuja 2")
        print("Digite 3 para usar el metodo de Burbuja 3")
        op2 = c.correcion()
        if(op2==1):
            print("=====Metodo de la burbuja #1=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja1(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        elif (op2==2):
            print("=====Metodo de la burbuja #2=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja2(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        elif(op2==3):
            print("=====Metodo de la burbuja #3=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja3(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        else:
            print("Opcion no disponinle")
    elif(opcion == 2):
        print("===== Metodo de Insercion =====")
        print(f"Numeros desordenados: {numeros2}")
        ins.ordenarInsercion(numeros2)
        print(f"Numeros desordenados: {numeros2}")
    elif(opcion==3):
        print("===== Metodo de Seleccion =====")
        print(f"Numeros Sin Ordenar: {numeros2}")
        s.ordenamienotSeleccion(numeros2)
        print(f"Numeros Ordenados: {numeros2}")
    elif(opcion==4):
        print("===== Metodo de Quicksort =====")
        print(f"Desordenados: {numeros2}")
        q.quicksort(numeros2, 0,len(numeros2)-1)
        print(f"Ordenados: {numeros2}")
    elif(opcion == 10):
        print("Gracias por usar el programa")
    elif(opcion == 5):
        print("===== Metodo de Shellsort =====")
        print(f"Desordenados: {numeros2}")
        ss.shellsort(numeros2)
        print(f"Ordenados: {numeros2}")
    elif(opcion == 6):
        print("===== Metodo de RadixSort =====")
        print(f"Desordenados: {numeros2}")
        r.radix(numeros2)
        print(f"Ordenados: {numeros2}")
    elif(opcion == 7):
        print("===== METODO DE INTERCALACION =====")
        print("Vectores:")
        print(f"{numeros3}")
        print(f"{aux2}")
        print(f"Fusion y ordenado {iii.interc(numeros3,aux2)}" )
    elif(opcion==8):
        print("===== Metodo de Mezcla Directa =====")
        print(f"Despredenado: {numeros2}")
        md.mezclaDirecta(numeros2)
        print(f"Ordenados: {numeros2}" )
    elif(opcion==9):
        print("===== Metodo de Mezcla Natural =====")
        print(f"Desordenado: {numeros2}")
        mn.mezclaNatural(numeros2)
        print(f"Ordenado: {numeros2}")
    else:
        print("Opcion no disponible")

        
'''               
print(f"Numeros Sin Ordenar: {numeros}")
s.ordenamienotSeleccion(numeros)
print(f"Numeros Ordenados: {numeros}")

numeros = [3,5,10,1,12,15,16]
print("Desdordenados: " + str(numeros))
ins.ordenarInsercion(numeros)
print("Ordenados: " + str(numeros))

numeros = [3,5,10,1,12,15,16]
print(f"Desordenados: {numeros}")
q.quicksort(numeros, 0,len(numeros)-1)
print(f"Ordenados: {numeros}")
'''



